VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form MainMenu 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Main Menu"
   ClientHeight    =   3930
   ClientLeft      =   4755
   ClientTop       =   3480
   ClientWidth     =   10920
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3930
   ScaleWidth      =   10920
   Begin VB.CommandButton btnKeluar 
      Caption         =   "Keluar"
      Height          =   495
      Left            =   8880
      TabIndex        =   4
      Top             =   3120
      Width           =   1455
   End
   Begin VB.CommandButton btnEdit 
      Caption         =   "Edit"
      Enabled         =   0   'False
      Height          =   495
      Left            =   6000
      TabIndex        =   3
      Top             =   3120
      Width           =   1335
   End
   Begin VB.CommandButton btnHapus 
      Caption         =   "Hapus"
      Enabled         =   0   'False
      Height          =   495
      Left            =   3240
      TabIndex        =   2
      Top             =   3120
      Width           =   1455
   End
   Begin VB.CommandButton btnSimpan 
      Caption         =   "Tambah"
      Height          =   495
      Left            =   600
      TabIndex        =   1
      Top             =   3120
      Width           =   1335
   End
   Begin MSFlexGridLib.MSFlexGrid gridNasabah 
      Height          =   2655
      Left            =   480
      TabIndex        =   0
      Top             =   120
      Width           =   9975
      _ExtentX        =   17595
      _ExtentY        =   4683
      _Version        =   393216
      ScrollTrack     =   -1  'True
   End
End
Attribute VB_Name = "MainMenu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub btnEdit_Click()
    FormInput.Show
    Unload Me
End Sub

Private Sub btnHapus_Click()
    hapus = MsgBox("yakin ingin menghapus data ini?", vbQuestion + vbYesNo, "Hapus")
    If hapus = vbYes Then
        SQL = "DELETE FROM nasabah WHERE id='" & idCarrier & "'"
        konekDb.Execute SQL, , adCmdText
        Call Form_Load
    End If
End Sub

Private Sub btnKeluar_Click()
    Unload Me
End Sub

Private Sub btnSimpan_Click()
    idCarrier = ""
    FormInput.Show
    Unload Me
End Sub

Private Sub Form_Load()
    Call bukaDatabase
    Call loadDataToGrid
End Sub

Sub initGrid()
    With gridNasabah
        .Cols = 6
        
        .RowHeightMin = 300
        .Col = 0
        .Row = 0
        .Text = "ID"
        .CellFontBold = True
        .ColWidth(0) = 500
        .AllowUserResizing = flexResizeColumns
        .CellAlignment = flexAlignCenterCenter
        
        .Col = 1
        .Row = 0
        .Text = "NAMA"
        .CellFontBold = True
        .ColWidth(1) = 2000
        .AllowUserResizing = flexResizeColumns
        .CellAlignment = flexAlignCenterCenter
        
        .Col = 2
        .Row = 0
        .Text = "TANGGAL LAHIR"
        .CellFontBold = True
        .ColWidth(2) = 1600
        .AllowUserResizing = flexResizeColumns
        .CellAlignment = flexAlignCenterCenter
        
        .Col = 3
        .Row = 0
        .Text = "JENIS KELAMIN"
        .CellFontBold = True
        .ColWidth(3) = 1500
        .AllowUserResizing = flexResizeColumns
        .CellAlignment = flexAlignCenterCenter
        
        .Col = 4
        .Row = 0
        .Text = "ALAMAT"
        .CellFontBold = True
        .ColWidth(4) = 2000
        .AllowUserResizing = flexResizeColumns
        .CellAlignment = flexAlignCenterCenter
        
        .Col = 5
        .Row = 0
        .Text = "PEKERJAAN"
        .CellFontBold = True
        .ColWidth(5) = 2000
        .AllowUserResizing = flexResizeColumns
        .CellAlignment = flexAlignCenterCenter
        
    End With
End Sub

Private Sub loadDataToGrid()
    Dim baris As Integer
    gridNasabah.Clear
    Call initGrid
    
    gridNasabah.Rows = 2
    baris = 0
    
    Set rsNasabah = New ADODB.Recordset
    rsNasabah.Open "SELECT * FROM nasabah", konekDb, adOpenDynamic, adLockOptimistic
    
    With rsNasabah
        Do While Not .EOF
            On Error Resume Next
            baris = baris + 1
            gridNasabah.Rows = baris + 1
            gridNasabah.TextMatrix(baris, 0) = !id
            gridNasabah.TextMatrix(baris, 1) = !nama
            gridNasabah.TextMatrix(baris, 2) = !tanggal_lahir
            gridNasabah.TextMatrix(baris, 3) = !jenis_kelamin
            gridNasabah.TextMatrix(baris, 4) = !alamat
            gridNasabah.TextMatrix(baris, 5) = !pekerjaan
            .MoveNext
        Loop
    End With
End Sub

Private Sub gridNasabah_Click()
    Dim gridBaris As Integer
    gridBaris = gridNasabah.Row
    
    btnHapus.Enabled = True
    btnEdit.Enabled = True
    
    Set rsNasabah = New ADODB.Recordset
    rsNasabah.Open "SELECT * FROM nasabah WHERE id='" & gridNasabah.TextMatrix(gridBaris, 0) & "'", _
        konekDb, adOpenDynamic, adLockOptimistic
        
    If rsNasabah.BOF Then
        MsgBox "TABEL MASIH KOSONG", vbOKOnly + vbInformation, "Perhatian"
        Exit Sub
    Else
        With rsNasabah
            On Error Resume Next
            idCarrier = !id
        End With
    End If
End Sub
