Attribute VB_Name = "koneksi_mysql"
Option Explicit
Public konekDb As New ADODB.Connection
Public rsNasabah As New ADODB.Recordset
Public idTerakhir As Integer

Public sqlUbah, sqlSimpan, sqlHapus, idCarrier As String

Sub bukaDatabase()
    Dim host, db, user, pass, port As String
    
    host = "localhost"
    port = "3306"
    db = "pkl_task1_db"
    user = "root"
    pass = ""
    
    Set konekDb = New ADODB.Connection
    konekDb.CursorLocation = adUseClient
        
    konekDb.ConnectionString = "" _
        & "DRIVER={MYSQL ODBC 3.51 Driver};" _
        & "SERVER=" & host & ";" _
        & "DATABASE=" & db & ";" _
        & "UID=" & user & ";" _
        & "PWD=" & pass & ";" _
        & "OPTION="
        
    On Error Resume Next
    If konekDb.State = adStateOpen Then
        konekDb.Close
        Set konekDb = New ADODB.Connection
        konekDb.Open
    Else
        konekDb.Open
    End If
    
    If Err.Number <> 0 Then
        MsgBox "GAGAL KONEKSI SERVER", vbOKOnly, "Kesalahan"
    End If
End Sub
