-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 21, 2013 at 11:39 AM
-- Server version: 5.1.28
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pkl_task1_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `nasabah`
--

CREATE TABLE IF NOT EXISTS `nasabah` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` text NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `jenis_kelamin` enum('laki-laki','perempuan') NOT NULL,
  `alamat` text NOT NULL,
  `pekerjaan` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `nasabah`
--

INSERT INTO `nasabah` (`id`, `nama`, `tanggal_lahir`, `jenis_kelamin`, `alamat`, `pekerjaan`) VALUES
(1, 'Kancil', '1990-10-06', 'laki-laki', 'Pamulang', 'Mahasiswa'),
(4, 'Badrun', '1994-10-22', 'laki-laki', 'Jakarta Utara', 'Mahasiswa'),
(5, 'Gandhi', '2013-10-21', 'laki-laki', 'India', 'Pemuka Agama'),
(7, 'Ruchdi Muttaqin', '1992-01-13', 'laki-laki', 'Pamulang', 'Mahasiswa'),
(8, 'Ariefman Zulpa', '1991-09-04', 'laki-laki', 'Ciledug', 'Mahasiswa'),
(13, 'Neji', '1991-08-27', 'laki-laki', 'Jepang', 'Ninja'),
(14, 'Anti', '2013-10-21', 'perempuan', 'Rempoa', 'Mahasiswi'),
(15, 'Badrun', '1990-10-13', 'laki-laki', 'Jakarta', 'Pegawai Swasta'),
(16, 'Tukiyem', '1992-06-09', 'perempuan', 'Brebes', 'Guru'),
(17, 'Alief', '1992-09-24', 'laki-laki', 'Pemalang', 'Mahasiswa'),
(18, 'Rianti', '2013-10-21', 'perempuan', 'Bandung', 'Selebritis');

-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

CREATE TABLE IF NOT EXISTS `user_account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `email` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_account`
--

INSERT INTO `user_account` (`id`, `username`, `password`, `email`) VALUES
(1, 'admin', 'admin123', 'admin@admin.com');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
