VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form FormInput 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Input Data"
   ClientHeight    =   4665
   ClientLeft      =   6765
   ClientTop       =   2805
   ClientWidth     =   6375
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4665
   ScaleWidth      =   6375
   Begin VB.CommandButton btnCancel 
      Caption         =   "Cancel"
      Height          =   495
      Left            =   4800
      TabIndex        =   13
      Top             =   3960
      Width           =   975
   End
   Begin VB.CommandButton btnSave 
      Caption         =   "Save"
      Height          =   495
      Left            =   3360
      TabIndex        =   12
      Top             =   3960
      Width           =   1095
   End
   Begin VB.TextBox txtPekerjaan 
      Height          =   285
      Left            =   2520
      TabIndex        =   11
      Top             =   3360
      Width           =   3255
   End
   Begin VB.TextBox txtAlamat 
      Height          =   285
      Left            =   2520
      TabIndex        =   9
      Top             =   2640
      Width           =   3255
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   2520
      TabIndex        =   7
      Top             =   960
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   661
      _Version        =   393216
      Format          =   3342339
      CurrentDate     =   41568
   End
   Begin VB.Frame Frame1 
      Height          =   615
      Left            =   2520
      TabIndex        =   4
      Top             =   1680
      Width           =   3015
      Begin VB.OptionButton Option2 
         Caption         =   "Perempuan"
         Height          =   195
         Left            =   1680
         TabIndex        =   6
         Top             =   240
         Width           =   1215
      End
      Begin VB.OptionButton Option1 
         Caption         =   "Laki-Laki"
         Height          =   195
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.TextBox txtNama 
      Height          =   285
      Left            =   2520
      TabIndex        =   3
      Top             =   360
      Width           =   3255
   End
   Begin VB.Label Label5 
      Caption         =   "Pekerjaan"
      Height          =   255
      Left            =   240
      TabIndex        =   10
      Top             =   3360
      Width           =   1095
   End
   Begin VB.Label Label1 
      Caption         =   "Alamat"
      Height          =   255
      Left            =   240
      TabIndex        =   8
      Top             =   2640
      Width           =   1335
   End
   Begin VB.Label Label4 
      Caption         =   "Jenis Kelamin"
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   1920
      Width           =   1695
   End
   Begin VB.Label Label3 
      Caption         =   "Tanggal Lahir"
      Height          =   255
      Left            =   240
      TabIndex        =   1
      Top             =   1080
      Width           =   1695
   End
   Begin VB.Label Label2 
      Caption         =   "Nama"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   360
      Width           =   1695
   End
End
Attribute VB_Name = "FormInput"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub clearInput()
    txtNama = ""
    Option1.Value = False
    Option2.Value = False
    DTPicker1.Value = Now
End Sub

Private Sub btnCancel_Click()
    MainMenu.btnEdit.Enabled = False
    MainMenu.btnHapus.Enabled = False
    MainMenu.Show
    Unload Me
End Sub

Private Sub btnEdit_Click()
        
    If txtNama.Text = "" Then
        MsgBox "ISI FORM YANG MASIH KOSONG", vbInformation, "Peringatan"
        Exit Sub
    End If
    
    If Option1.Value = False Then
        If Option2.Value = False Then
            MsgBox "ISI FORM YANG MASIH KOSONG", vbInformation, "Peringatan"
            Exit Sub
        End If
    End If
      
    Dim jenisKelamin As String
    If Option1.Value = True Then
        jenisKelamin = "laki-laki"
    Else
        jenisKelamin = "perempuan"
    End If
    
    Dim tanggalLahir As String
    tanggalLahir = Format(DTPicker1.Value, "yyyy/MM/dd")
        
    sqlUbah = "UPDATE nasabah set nama='" & txtNama.Text & "', tanggal_lahir='" & tanggalLahir & "'," _
        & " jenis_kelamin='" & jenisKelamin & "' WHERE id='" & txtId.Text & "'"
    konekDb.Execute sqlUbah, , adCmdText
    
    Call loadDataToGrid
    Call clearInput
End Sub

Private Sub btnHapus_Click()

    Set rsNasabah = New ADODB.Recordset
    rsNasabah.Open "SELECT * FROM nasabah WHERE id='" & txtId.Text & "'", _
        konekDb, adOpenDynamic, adLockBatchOptimistic
        
    If rsNasabah.BOF Then
        MsgBox "Data yang akan dihapus tidak ada", vbInformation + vbOKOnly, "Pemberitahuan"
        Exit Sub
    End If
        
    hapus = MsgBox("yakin ingin menghapus data ini?", vbQuestion + vbYesNo, "Hapus")
    If hapus = vbYes Then
        SQL = "DELETE FROM nasabah WHERE id='" & txtId.Text & "'"
        konekDb.Execute SQL, , adCmdText
        
        'rsNasbah.Requery
        Call clearInput
        Call Form_Load
    Else
    Call clearInput
End If
End Sub

Private Sub btnKeluar_Click()
    Unload Me
End Sub

Private Sub btnSave_Click()
    Dim tanggalLahir As String
    tanggalLahir = Format(DTPicker1.Value, "yyyy/MM/dd")
    
    If txtNama.Text = "" Then
        MsgBox "ISI FORM YANG MASIH KOSONG", vbInformation, "Peringatan"
        Exit Sub
    End If
    
    If Option1.Value = False Then
        If Option2.Value = False Then
            MsgBox "ISI FORM YANG MASIH KOSONG", vbInformation, "Peringatan"
            Exit Sub
        End If
    End If
    
    If txtAlamat.Text = "" Then
        MsgBox "ISI FORM YANG MASIH KOSONG", vbInformation, "Peringatan"
        Exit Sub
    End If
    
    If txtPekerjaan.Text = "" Then
        MsgBox "ISI FORM YANG MASIH KOSONG", vbInformation, "Peringatan"
        Exit Sub
    End If
    
    Dim jenisKelamin As String
    If Option1.Value = True Then
        jenisKelamin = "laki-laki"
    Else
        jenisKelamin = "perempuan"
    End If
    
    If idCarrier = "" Then
        sqlSimpan = "INSERT INTO nasabah (nama, tanggal_lahir, jenis_kelamin, alamat, pekerjaan)" _
            & " VALUES('" & txtNama.Text & "','" & tanggalLahir & "'" _
            & ",'" & jenisKelamin & "','" & txtAlamat.Text & "','" & txtPekerjaan & "')"
        konekDb.Execute sqlSimpan, , adCmdText
        MsgBox "DATA BARU TELAH TERSIMPAN", vbOKOnly + vbInformation, "INFO"
    Else
        sqlUbah = "UPDATE nasabah set nama='" & txtNama.Text & "', tanggal_lahir='" & tanggalLahir & "'," _
            & " jenis_kelamin='" & jenisKelamin & "', alamat='" & txtAlamat.Text & "', pekerjaan='" _
            & "" & txtPekerjaan & "' WHERE id='" & idCarrier & "'"
        konekDb.Execute sqlUbah, , adCmdText
        MsgBox "DATA TELAH DI UBAH", vbOKOnly + vbInformation, "INFO"
    End If
    
    MainMenu.Show
    Unload Me
End Sub

Private Sub btnSimpan_Click()
    Set rsNasabah = New ADODB.Recordset
    rsNasabah.Open "SELECT * FROM nasabah WHERE id='" & txtId.Text & "'", _
        konekDb, adOpenDynamic, adLockBatchOptimistic
        
    If txtNama.Text = "" Then
        MsgBox "ISI FORM YANG MASIH KOSONG", vbInformation, "Peringatan"
        Exit Sub
    End If
    
    If Option1.Value = False Then
        If Option2.Value = False Then
            MsgBox "ISI FORM YANG MASIH KOSONG", vbInformation, "Peringatan"
            Exit Sub
        End If
    End If
      
    Dim jenisKelamin As String
    If Option1.Value = True Then
        jenisKelamin = "laki-laki"
    Else
        jenisKelamin = "perempuan"
    End If
    
    Dim tanggalLahir As String
    tanggalLahir = Format(DTPicker1.Value, "yyyy/MM/dd")
    
    'If rsNasabah.BOF Then
        sqlSimpan = ""
        sqlSimpan = "INSERT INTO nasabah (nama, tanggal_lahir, jenis_kelamin)" _
            & " VALUES('" & txtNama.Text & "','" & tanggalLahir & "" _
            & "','" & jenisKelamin & "')"
        konekDb.Execute sqlSimpan, , adCmdText
        Call Form_Load
        MsgBox "DATA BARU TELAH TERSIMPAN", vbOKOnly + vbInformation, "INFO"
    'End If
    rsNasabah.Requery
    Call clearInput
End Sub


Private Sub Form_Load()
    Call bukaDatabase
    
    If idCarrier = "" Then
    
    Else
        Set rsNasabah = New ADODB.Recordset
        rsNasabah.Open "SELECT * FROM nasabah WHERE id='" & idCarrier & "'", _
            konekDb, adOpenDynamic, adLockBatchOptimistic
            
        With rsNasabah
            On Error Resume Next
            txtNama.Text = !nama
            DTPicker1.Value = !tanggal_lahir
            Dim jenisKelmain As String
            jenisKelmain = !jenis_kelamin
            If jenisKelmain = "laki-laki" Then
                Option1.Value = True
            Else
                Option2.Value = True
            End If
            txtAlamat.Text = !alamat
            txtPekerjaan.Text = !pekerjaan
        End With
    End If
    
    DTPicker1.Value = Date
End Sub

